import axios from 'axios';

class HttpService {
  static get(url) {
    return HttpService.send(encodeURI(url));
  }

  static send(url) {
    return axios.get(url).then(HttpService.response);
  }

  static response = (payload) => {
    return payload.data;
  };

  static getPromises(urlArray){
    return urlArray.map(url => HttpService.get(url));
  }
}

export default HttpService;