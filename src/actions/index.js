import HttpService from "../services/HttpService";
import axios from 'axios';

const loadData = (urlArray) => (dispatch) => {
  dispatch({
    type: 'SEND_DATA_REQUEST',
    payload: {
      loading: true,
    }
  });

  const promises = HttpService.getPromises(urlArray);

  axios.all(promises)
      .then(axios.spread((...responses) => {
        responses.forEach(res => {
          dispatch({
            type: 'GET_DATA_SUCCESS',
            payload: {
              image: res.message
            },
          });
        });
        dispatch({
          type: 'GET_ALL_DATA_SUCCESS',
          payload: {
            loading: false
          },
        });
      }))
      .catch(error => {
      })
};

export default loadData;