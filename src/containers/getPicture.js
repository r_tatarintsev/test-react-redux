import React from 'react';
import { connect } from 'react-redux';

const GetPicture = (props) => (
    (props.loading) ? <div>Loading...</div> : (
        <div>
          {props.images.map((src, index) => (
              <div className='img-block' key={index}><img alt='' src={src} /></div>
          ))}
        </div>
    )
);

const mapStateToProps = (store) => {
  return {
    loading: store.loading,
    images: store.images
  }
};

export default connect(mapStateToProps)(GetPicture);