import React from 'react';
import PropTypes from 'prop-types';

function Content(props) {
  return (
      <div>
        <h1>{props.title}</h1>
        <p>{props.text}</p>
      </div>
  );
}

Content.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
};

export default Content;