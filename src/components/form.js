import React, { useState } from 'react';
import loadData from "../actions";
import store from '../store/store';

const Form = () => {

  const [count, setCount] = useState(0);
  const max_count = 5;
  const url = 'https://dog.ceo/api/breeds/image/random';

  const onClickHandler = () => {
    let urlArray = [];
    for (let i = 0; i < count; i++) {
      urlArray.push(url);
    }
    store.dispatch(loadData(urlArray));
  };

  return (
      <>
        <input className='input-text' type='text' onChange={(e) => setCount(e.target.value)}/>
        <button className='button' disabled={isNaN(count) || count > max_count || count < 1} onClick={onClickHandler}>Send</button>
        {count > max_count && <div className='error'>Значение должно быть не больше {max_count}.</div>}
      </>
  );
};

export default Form;
