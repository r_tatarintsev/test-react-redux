import React from 'react';
import './App.css';
import Content from './components/content';
import Form from './components/form';
import GetPicture from './containers/getPicture'

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <Content title={'Hello, World'} text={'Lorem ipsum'} />
                <Form />
                <GetPicture />
            </header>
        </div>
    );
}

export default App;
