import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";

const defaultState = {
  loading: false,
  images: [],
};

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'SEND_DATA_REQUEST':
      return {
        loading: action.payload.loading,
        images: []
      };

    case 'GET_DATA_SUCCESS':
      return {
        images: [...state.images, action.payload.image],
      };

    case 'GET_ALL_DATA_SUCCESS':
      return {
        loading: action.payload.loading,
        images: [...state.images]
      };

    default:
      return state;
  }
};

const store = createStore(reducer, applyMiddleware(thunk));

export default store;